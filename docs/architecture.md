# architecture

The architecture of DIOS has undergone several significant reconfigurations, but
this document will attempt to reflect the current state of the software.

![DIOS architecture](./architecture.jpg)

DIOS is not a monolithic application; rather it is made up of several
components:

- a **web application** written in [Rust](https://www.rust-lang.org/) that provides
  the user interface, chat server, and access control
- an instance of [OME (OvenMediaEngine)](https://github.com/AirenSoft/OvenMediaEngine) configured
  as an **RTMP server**
- a **PostgreSQL database** for persisting user data
- an NGINX **reverse proxy/ingress** for routing incoming requests

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [architecture](#architecture)
    - [rust web application](#rust-web-application)
        - [user model](#user-model)
        - [video player](#video-player)
        - [chat server](#chat-server)
    - [OME as an RTMP server](#ome-as-an-rtmp-server)
        - [authentication](#authentication)
    - [PostgreSQL](#postgresql)
    - [S3](#s3)
    - [reverse proxy/ingress](#reverse-proxyingress)

<!-- markdown-toc end -->

## rust web application

The Rust web application comprises the primary original development work of this
project; it glues the other components together and provides a custom web
interface for the stream.  As a result, “DIOS” may sometimes be used to refer
specifically to this component of the system.

The Rust code exists in the [`/src`](../src) directory of this repository.

### user model

Users in DIOS are defined as `structs` and are persisted in and queried from
PostgreSQL by the [Diesel ORM](https://diesel.rs/).  The model definition exists
in [`src/models.rs`](../src/models.rs).

When someone attempts to sign up for a user account, a new `User` object is
created, but is initially de-activated until the person signing up completes the
email confirmation process.  (Email confirmation can be disabled by setting the
`SMTP_ENABLED` environment variable to `false`, but this isn’t recommended in
production.)

Only users with the `streamer` property set to `true` are authorized to stream
on DIOS.

### video player

When a user streams to DIOS via [OBS](https://obsproject.com/) or similar
software, they do so over RTMP.  RTMP is a protocol, not a video format.  The
RTMP server (OME) takes the stream from OBS and converts it to an
[HLS-compatible](https://en.wikipedia.org/wiki/HTTP_Live_Streaming) media
playlist.  These playlists (which have the `.m3u8` extension) can be loaded by
an HTML `<video>` element in a web page.  However, not all browsers currently
have native support for HLS.  Therefore we use a [JavaScript
library](https://github.com/AirenSoft/OvenPlayer) to ensure that the playlist
can be loaded by most modern browsers.

### chat server

TODO

## OME as an RTMP server

OME is an open-source project that provides a video streaming server that
supports multiple protocols and formats in addition to RTMP.  OME also includes
an HTTP server in order to serve the HLS playlist.

### authentication

When a user attempts to stream to DIOS, we have to ensure that they are a user
with streaming privileges.  This is accomplished with HTTP hooks defined in the
[OME
configuration](https://airensoft.gitbook.io/ovenmediaengine/access-control/admission-webhooks#configuration).

When a user attempts to begin streaming, OME sends an `opening` request to the
`/on_connect` endpoint of the Rust server.  This request includes the users’
(private) stream key, which DIOS uses to look up the user and verify whether or
not they are allowed to stream.  If they are, the stream begins; if not, the
connection is closed.

When a user ends their stream, OME sends a `closing` request to `/on_connect`.
This tells DIOS to mark them as offline.

## PostgreSQL

PostgreSQL is used only for persisting user data.  The Rust
application code interfaces with it using the Diesel ORM.  OME does not interact
with PostgreSQL.

As of now we have no way, in the web interface, to manage which users have
permission to stream.  Instead it is handled with a shell script that is run on
the Rust server itself:

```shell
/dios/scripts/admin.sh 'user@email.com'   # grant streaming permissions
/dios/scripts/unadmin.sh 'user@email.com' # revoke streaming permissions
```

## S3

Image assets (the offline poster, stickers) are stored in S3.  Before deploying,
create buckets in an S3-compatible server and configure them in your
`values.yaml` overrides.

When developing locally with Docker Compose, a MinIO instance will be created
for you.

**Note that the Rust S3 library [does not support the subdomain style of
addressing](https://github.com/rusoto/rusoto/issues/1482), so you will have to
use an S3 service that supports the older path style.**

```yaml
dios:
  app_domain: your.cool.tld
  app_title: 'not twitch'
  s3:
    use_https: true
    assets:
      access_key: cool-key
      access_secret: cool-lock
      bucket: assets
      endpoint: https://not.aws
      region: cool-zone
    clips:
      enabled: true
      access_key: cooler-key
      access_secret: cooler-still
      bucket: clips
      endpoint: https://ewr1.vultrobjects.com
      host: ewr1.vultrobjects.com
      region: ewr1
```

If you enable clips, it is strongly recommended that you set an aggressive
[lifecycle
policy](https://www.linode.com/docs/products/storage/object-storage/guides/lifecycle-policies/)
on them, to avoid storage charges; consider something like the following:

```xml
<LifecycleConfiguration>
    <Rule>
        <ID>delete-segments</ID>
        <Filter>
        <Prefix>1</Prefix>
        </Filter>
        <Status>Enabled</Status>
        <Expiration>
            <Days>1</Days>
        </Expiration>
    </Rule>
    <Rule>
        <ID>delete-clips</ID>
        <Filter>
        <Prefix>clip-</Prefix>
        </Filter>
        <Status>Enabled</Status>
        <Expiration>
            <Days>7</Days>
        </Expiration>
    </Rule>
</LifecycleConfiguration>
```

## reverse proxy/ingress

In our currently deployment configuration, none of the above components are
accessible from the public internet.  Instead they are deployed behind an NGINX
server that routes requests from the outside to the appropriate service:

- RTMP traffic on port 1935 (configurable) are sent to the OME ingest
- HTTP requests to `/live` (for the the HLS playlist) are routed to the OME HTTP
  server
- all other HTTP traffic is sent to the Rust application server
- no outside access to PostgreSQL is allowed

The architecture diagram from above, again:

![DIOS architecture](./architecture.jpg)
