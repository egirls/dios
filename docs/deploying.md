# deploying DIOS

Orchestrating the deployment of a multiple-component application is complicated,
and doing it in Kubernetes makes some things easier and some harder.  It allows
us continuous deployment, pushing out the latest changes as soon as they’re
merged with no downtime.  We can also provide ephemeral, review deployments for
merge requests, allowing visual review of changes without having to build locally.

The main downside involves how Kubernetes handles non-HTTP traffic.

## opening port 1935

By default, RTMP uses port 1935, although OME can be configured to listen on a
different port.  Whichever one you use will have to be manually opened from
within whichever [ingress
controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
you’re using.  We use the [Kubernetes community NGINX ingress
controller](https://kubernetes.github.io/ingress-nginx/) (not to be confused
with the ingress controller developed by the NGINX corporation), and the
following instructions will be specific to that controller.

### configuring the ingress controller

Unfortunately, Kubernetes support for non-HTTP traffic, both TCP and UDP, is
underdeveloped.  Our ingress controller [cannot dynamically route such traffic
between
services](https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/),
as it does with HTTP traffic.  The most we can do is configure the ingress
controller to always route traffic from a specific port to a specific service.
This means that if you want to run multiple instances of DIOS in a single
cluster, **each will need OME/RTMP to be listening on a different port**.  For
example, deploying the ingress controller with the following additional values
will allow two different DIOS instances to run, one listening on 1935 and the
other on 1936:

```yaml
tcp:
  1935: "dios-production/dios-ingest:1935"
  1936: "dios-testing/dios-test-ingest:1936"
```
## environment variables

All configurable settings in DIOS are managed with environment variables.  The
default values listed in [`values.yaml`](../chart/values.yaml) correspond to the
variables listed in [`.env.template`](../.env.template).  This prevents us from
hardcoding configuration options into the code, and simplifies multiple
deployments.

## HTTPS

HTTPS should be enabled in production.  We use [cert-manager] for our
deployments, with the following ingress configuration:

```yaml
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  hosts:
    - host: live.bimbo.systems
      paths: ['/']
  tls:
   - secretName: live-tls
     hosts:
       - live.bimbo.systems
```

(If your [`ClusterIssuer`](https://cert-manager.io/docs/concepts/issuer/) is not
named `letsencrypt-prod`, or if you use a different certificate manager
entirely, you will need to override these values.)
