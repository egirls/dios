#!/bin/sh

PG_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}"

STICKERS="${DATA_ROOT}/stickers"

for dir in "$STICKERS"/*; do
  if [ ! -d "$dir" ]; then
    echo "$dir is not a directory, skipping"
  else
    STICKER_PACK=$(basename "$dir")
    echo "Migrating sticker pack \"$STICKER_PACK\""
    for img in "$dir"/png/*; do
      STICKER_ID="$(uuidgen)"
      STICKER_NAME=$(basename "$img" ".png")
      echo "Found $img, will be copied to $STICKERS/$STICKER_ID"
      cp "$img" "$STICKERS/$STICKER_ID"

      echo "Creating DB entry for:
             id:   $STICKER_ID
             pack: $STICKER_PACK
             name: $STICKER_NAME"

      psql "$PG_URL" -c "insert into stickers (id, pack, name) values ('$STICKER_ID', '$STICKER_PACK', '$STICKER_NAME');"
    done
  fi
done

echo "-- Thank you, the stickies have been migrated.
   They will not appear in the generated JSON until a change is made from the UI."
