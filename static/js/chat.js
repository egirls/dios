import twemoji from 'twemoji'

(async function() {
  let conn = null
  const reconnect_time_base = 1000
  let reconnect_time = reconnect_time_base
  let reconnect_mult = 2
  let colors = {}
  let chat_scrolled = false
  let observer = null
  const bigmoji_height_limit = 200

  let CUSTOMS = []
  let STICKERS = {}
  const EMOJI_REGEX = /:([a-z0-9]+):/ig

  try {
    const stickerJSON = await fetch('stickers/stickers.json', { cache: 'no-cache' })
    STICKERS = await stickerJSON.json()
  } catch ( error ) {
    console.log("Error trying to fetch stickers: ", error )
  }

  //sticker and sticker picker variable
  const min_search_length = 2
  const STICKER_REGEX = /^\s*:(?<pack>[a-z0-9-_]+)\/(?<sticker>[a-z0-9-_]+):\s*$/i
  const STICKERS_REGEX = /\s*:(?<pack>[a-z0-9-_]+)\/(?<sticker>[a-z0-9-_]+):\s*/ig

  const DARK_BACKGROUND = "#000000"
  const DARK_TEXT = "#ffffff"
  const LIGHT_BACKGROUND = "#ffffff"
  const LIGHT_TEXT = "#000000"
  const MIN_CONTRAST = 5

  // Picker DOM Manipulation
  function show_pack  (id) {
    document.getElementById("pack-list").style.display = 'none';
    document.getElementById(id).style.display = 'flex';
  }

  function show_pack_list (id) {
    document.getElementById("pack-list").style.display = 'flex';
    document.getElementById(id).style.display = 'none';
  }

  function add_text(pack, id) {
    let text_elem = document.getElementById('text');

    text_elem.value += `:${pack}/${id}:`
  }

  // Picker Event Handlers
  function handle_virtual_pack_name(event) {
    let name = event.target.textContent.replace(">", "").replace(" ", "-");

    let picker = document.getElementById("picker");

    if (document.getElementById(`pack-${name}`) === null) {
      create_virtual_pack(picker, name)
      console.log("Created pack.")
    }

    show_pack(`pack-${name}`);
  }

  function handle_pack_name(event) {
    let name = event.target.textContent.replace(">", "").replace(" ", "-");

    let picker = document.getElementById("picker");

    if (document.getElementById(`pack-${name}`) === null) {
      create_pack(picker, name,STICKERS[name])
      console.log("Created pack.")
    }

    show_pack(`pack-${name}`);
  }

  function handle_search_input(event) {
    show_pack(`pack-SEARCH`);

    let search_stickers = document.querySelector('#pack-SEARCH div.stickers')
    search_stickers.textContent = ''

    for (let pack_name of Object.keys(STICKERS)) {
      for (let sticker of STICKERS[pack_name]) {
        if (sticker.name.toLowerCase() === event.target.value.toLowerCase()) {

          let new_sticker_element = document.createElement("template")
          new_sticker_element.innerHTML = `<img class="sticker" data-pack="${pack_name}" data-name=${sticker.name} src="/stickers/${sticker.id}">`

          new_sticker_element.content.firstChild.onclick = handle_click_sticker

          search_stickers.appendChild(new_sticker_element.content.firstChild);
        }
      }
    }
    if (event.target.value.length >= min_search_length) {

      for (let pack_name of Object.keys(STICKERS)) {
        for (let sticker of STICKERS[pack_name]) {
          let inclusion = sticker.name.toLowerCase().includes(event.target.value.toLowerCase())
          let exact = sticker.name.toLowerCase() === event.target.value.toLowerCase()

          if (inclusion && !exact) { //Exact matches are already included
            let new_sticker_element = document.createElement("template")
            new_sticker_element.innerHTML = `<img class="sticker" data-pack="${pack_name}" data-name=${sticker.name} src="/stickers/${sticker.id}">`

            new_sticker_element.content.firstChild.onclick = handle_click_sticker

            search_stickers.appendChild(new_sticker_element.content.firstChild);
          }
        }
      }
    }
  }

  function focus_text() {
    document.getElementById("text").focus()
  }


  function close_picker() {
    const picker_parent = document.getElementById("picker-parent")
    if(picker_parent.attributes.getNamedItem("open") != null)
      picker_parent.attributes.removeNamedItem("open")
  }

  function handle_picker_focusout(event) {
    const picker_parent = document.getElementById("picker-parent")
    if(!picker_parent.contains(event.target))
        close_picker()
  }

  function handle_picker_keydown(event) {
    console.log(event.key)
    console.log(event.target.id)
    if (event.key === 'Escape' && event.target.id === 'search-input') {
        if(event.target.value) {
          event.target.value = ""
          show_pack_list("SEARCH")
        } else {
          close_picker()
        }
    }
    else if (event.key === 'Escape') {
      close_picker()
    }
  }

  function handle_picker_toggle(/*event*/) {
    const picker_parent = document.getElementById("picker-parent")
    if(picker_parent.open) {
        document.getElementById("search-input").focus()
    }
  }

  function handle_back(event) {
    let name = event.target.parentElement.id;

    show_pack_list(name);
  }

  function handle_click_sticker(event) {
    let sticker_name = event.target.dataset.name;
    let pack_name = event.target.dataset.pack;

    add_text(pack_name, sticker_name);
    close_picker()
    focus_text()
  }

  // creates a pack based on a group of stickers from stickers.json
  function create_pack(picker, pack_name, pack_stickers) {
      let new_element = document.createElement("template")
      let pack_name_id = pack_name.replace(" ", "-")
    console.log(`creating pack ${pack_name}`)

      new_element.innerHTML = `<div class="pack" data-name="${pack_name}" id="pack-${pack_name_id}"><div class="back">${pack_name}</div><div class="stickers"></div></div>`

      new_element = new_element.content.firstChild
      if (pack_stickers != null) {
        for (let sticker of pack_stickers) {
          let new_sticker_element = document.createElement("div")
          new_sticker_element.innerHTML = `<img class="sticker" data-pack="${pack_name}" data-name="${sticker.name}" src="/stickers/${sticker.id}" title=":${pack_name}/${sticker.name}:" alt=":${pack_name}/${sticker.name}:">`

          new_sticker_element.firstChild.onclick = handle_click_sticker

          new_element.lastChild.appendChild(new_sticker_element.firstChild);
        }
      }
      let new_back_element = document.createElement("template")
      new_back_element.innerHTML = "<div class='back'>&lt;back</div>"
      new_element.appendChild(new_back_element.content.firstChild)

      for (let back of new_element.getElementsByClassName("back"))
        back.onclick = handle_back

      picker.appendChild(new_element);
  }

  // creates a 'virtual pack'---one that includes stickers from every pack
  // specified in stickers.json
  function create_virtual_pack(picker, pack_name) {
      let new_element = document.createElement("template")
      let pack_name_id = pack_name.replace(" ", "-")

      new_element.innerHTML = `<div class="pack" data-name="${pack_name}" id="pack-${pack_name_id}"><div class="back">${pack_name}</div><div class="virtual-packs"></div></div>`

      new_element = new_element.content.firstChild
      for (let pack of Object.keys(STICKERS).sort()) {
        if(STICKERS[pack].length) {
          let holder = document.createElement('div')
          holder.innerHTML = `<div class="back">${pack}</div><div class="virtual-stickers"></div>`
          for (let sticker of STICKERS[pack]) {
              let new_sticker_element = document.createElement("div")
              new_sticker_element.innerHTML = `<img class="sticker" data-pack="${pack}" data-name="${sticker.name}" src="/stickers/${sticker.id}" title=":${pack}/${sticker.name}:" alt=":${pack}/${sticker.name}:">`

              new_sticker_element.firstChild.onclick = handle_click_sticker

              holder.lastChild.appendChild(new_sticker_element.firstChild);
          }
          new_element.lastChild.appendChild(holder)
        }
      }
      let new_back_element = document.createElement("template")
      new_back_element.innerHTML = "<div class='back'>&lt;back</div>"
      new_element.appendChild(new_back_element.content.firstChild)

      for (let back of new_element.getElementsByClassName("back"))
        back.onclick = handle_back

      picker.appendChild(new_element);
  }

  // sets up the sticker picker.
  async function setup_picker() {
    // add handlers
    for (let item of document.getElementsByClassName("name")) {
      item.onclick = handle_pack_name;
    }

    for (let item of document.getElementsByClassName("back")) {
      item.onclick = handle_back;
    }

    for (let item of document.getElementsByClassName("sticker")) {
      item.onclick = handle_click_sticker;
    }

    document.getElementById("chat").addEventListener("click",handle_picker_focusout, true)
    document.getElementById("picker").addEventListener("keydown",handle_picker_keydown, true)
    document.getElementById("picker-parent").addEventListener("toggle",handle_picker_toggle, true)
    document.getElementById("search-input").addEventListener("input", handle_search_input, true)

    //create Elements

    let pack_name_parent = document.getElementById("pack-list");

    //create virtual packs
    for( let pack_name of ['EVERYTHING']) {
      let new_element = document.createElement("template")

      new_element.innerHTML = `<div class="name"><span class="pack-name">${pack_name}</span>&gt;</div>`;
      new_element.content.firstChild.onclick = handle_virtual_pack_name;
      pack_name_parent.appendChild(new_element.content.firstChild);
    }
    for (let pack_name of Object.keys(STICKERS).sort()) {
      if(STICKERS[pack_name].length) {
        let new_element = document.createElement("template")

        new_element.innerHTML = `<div class="name"><span class="pack-name">${pack_name}</span>&gt;</div>`;
        new_element.content.firstChild.onclick = handle_pack_name;
        pack_name_parent.appendChild(new_element.content.firstChild);
      }
    }
    //add pack for search.
    for (let pack_name of ['SEARCH']) {
        let new_element = document.createElement("template")

        new_element.innerHTML = `<div class="name"><span class="pack-name">${pack_name}</span>&gt;</div>`;
        new_element.content.firstChild.onclick = handle_pack_name;
        pack_name_parent.appendChild(new_element.content.firstChild);
    }

    create_pack(picker, 'SEARCH',[])
  }

  // Chat functions

  function get_username_color() {
    let letters = '0123456789ABCDEF'
    let color = '#'
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)]
    }

    const lights = document.getElementById("toggle-lights").checked

    if (contrast(color, lights ? LIGHT_BACKGROUND : DARK_BACKGROUND) < MIN_CONTRAST) {
      color = get_username_color()
    }

    return color
  }

  function luminance(color) {
    const r = parseInt(color.slice(1, 3), 16)
    const g = parseInt(color.slice(3, 5), 16)
    const b = parseInt(color.slice(5, 7), 16)

    let a = [r, g, b].map(function (v) {
      v /= 255
      return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4)
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722
  }

  function contrast(color1, color2) {
    const lum1 = luminance(color1)
    const lum2 = luminance(color2)
    const brightest = Math.max(lum1, lum2)
    const darkest = Math.min(lum1, lum2)

    const contrast = (brightest + 0.05) / (darkest + 0.05)

    return contrast
  }

  function enbiggen(msgContainer) {
    if(!msgContainer.classList.contains('smallmoji') && !msgContainer.innerText.replace(/\s/g,'').length) {
      let emojos = msgContainer.querySelectorAll('img.emoji')
      for (let i = 0; i < emojos.length; i++) {
        emojos[i].classList.add('bigmoji')
      }
    }
  }
  function enshrinken(msgContainer) {
    let emojos = msgContainer.querySelectorAll('.bigmoji')
    for (let i = 0; i < emojos.length; i++) {
      emojos[i].classList.remove('bigmoji')
    }
  }
  function render_emoji(message) {
    let rendered_string = message

    const sticker_match = rendered_string.match(STICKER_REGEX)
    if(sticker_match && STICKERS.hasOwnProperty(sticker_match.groups.pack)) {
      const stickers = STICKERS[sticker_match.groups.pack].filter(stick => { return stick.name == sticker_match.groups.sticker })
      if(stickers.length) {
          rendered_string = `<img class="sticker" src="/stickers/${stickers[0].id}" alt=":${sticker_match.groups.pack}/${sticker_match.groups.sticker}:" title=":${sticker_match.groups.pack}/${sticker_match.groups.sticker}:">`
          return rendered_string
      }
    }

    const stickers = {};

    [...rendered_string.matchAll(STICKERS_REGEX)].forEach( match => {
      if( STICKERS.hasOwnProperty(match.groups.pack)) {
        if ( !stickers.hasOwnProperty(match.groups.pack))
          stickers[match.groups.pack] = new Set()
        stickers[match.groups.pack].add(match.groups.sticker)
      }
    })

    Object.keys(stickers).forEach(pack => {
      STICKERS[pack]
        .filter( stick => stickers[pack].has(stick.name))
        .forEach( sticker_match => {
          let sticker = `<img class="emoji" src="/stickers/${sticker_match.id}" alt=":${pack}/${sticker_match.name}:" title=":${pack}/${sticker_match.name}:">`
          rendered_string = rendered_string.replaceAll(`:${pack}/${sticker_match.name}:`, sticker)
        })
    })

    const emojos = [...new Set(
      [...rendered_string.matchAll(EMOJI_REGEX)].map(match => { return match[1] })
    )]

    CUSTOMS.filter(emoj => { return emojos.includes(emoj.name) }).forEach(match => {
      let emoji = `<img class="emoji" src="${match.emoji}" alt=":${match.name}:" title=":${match.name}:">`
      rendered_string = rendered_string.replaceAll(`:${match.name}:`, emoji)
    })

    return rendered_string
  }

  function clear_all() {
   let message_elements = document.getElementsByClassName("chat-message text");

    Array.
      from(message_elements).
      filter(elem => elem.getElementsByClassName("chat-username")[0].textContent != "System").
      forEach(elem => elem.remove());
  }

  function log(msg, user, time, id) {
    if (typeof id === "undefined") {
      id = Math.floor(Math.random() * 10000)
    }
    if(document.getElementById(`msg-${id}`))
        return;

    if (typeof user === "undefined") {
      user = "System"
    }
    if (typeof time === "undefined") {
      time = new Date(Date.now())
    }
    const locale = navigator.languages != undefined ? navigator.languages[0] : navigator.language
    const timestamp = time.toLocaleTimeString(
      locale,
      { timeStyle: "short", }
    )

    const nativeControl = document.querySelector('#log')

    const pause = !document.getElementById("toggle-pause").checked
    const bigmoji = document.getElementById("toggle-bigmoji").checked
    const atBottom = !chat_scrolled

    let timeStamp = `<time datetime="${time.toISOString()}" class="chat-timestamp">&lt${timestamp}&gt</time> `
    let userInfo = `<span class="chat-username" style="color:${colors[user]}"></span>: `
    let messageString = `<span id="msg-${id}" class="chat-message-text"></span>`

    let newElement = document.createElement('p')

    newElement.classList.add('chat-message')
    newElement.classList.add('text')
    newElement.classList.add('stealthy')
    newElement.id = `${id}`
    newElement.innerHTML = timeStamp + userInfo + messageString


    const rendered = render_emoji(msg)
    const msgContainer = newElement.querySelector(`#msg-${id}`)

    msgContainer.innerHTML = rendered
    msgContainer.previousElementSibling.textContent = user
    twemoji.parse(msgContainer, {
      folder:'svg',
      ext:'.svg'
    })

    if(bigmoji) {
      enbiggen(msgContainer)
    }

    nativeControl.insertBefore(newElement,document.getElementById('log-pixel'))

    if(newElement.offsetHeight > bigmoji_height_limit) {
      enshrinken(msgContainer)
      msgContainer.classList.add('smallmoji')
    }


    // we can run into issues with things that work off the message element's layed out height because images
    // can load in asynchronously after we've already done our smallmoji/scroll checks.
    //
    // work around this by stuffing a function to redo that work into the onload handler of every img that isn't
    // already loaded so we recheck whenever the element might have secretly gotten taller.
    //
    // technically we only need to call the callback and do the work for the very last image to be loaded in, but
    // it should be ~safe this way and is way way easier than having the callback do the extra work to recheck if
    // everything is loaded before reclassing/scrolling.

    const shrink_scroll = function() {
      if(newElement.offsetHeight > bigmoji_height_limit) {
        enshrinken(msgContainer)
        msgContainer.classList.add('smallmoji')
      }
      if (atBottom || !pause) {
        nativeControl.scrollTop = nativeControl.scrollHeight
      }
    }

    let emojos = msgContainer.querySelectorAll('img.emoji , img.sticker')
    for (let i = 0; i < emojos.length; i++) {
      const emojo = emojos[i]
      if(!emojo.complete) {
        // add event listener as 'once' so references to the closure should clean themselves up eventually
        //
        // TODO this is technically unsafe i thiiiiink because it's possible the img actually completed between
        // the line above and this one ( async completion ) and this will therefor never be invoked/cleaned up
        //
        // we could recheck and unlisten immediately afterwards if we cared but i simply do not
        emojo.addEventListener('load',shrink_scroll,{'once':true})
      }
    }
    newElement.classList.remove('stealthy')

    if (atBottom || !pause) {
      nativeControl.scrollTop = nativeControl.scrollHeight
    }
  }

  function connect() {
    const wsUri = (window.location.protocol == 'https:' && 'wss://' || 'ws://') + window.location.host + '/ws/'
    conn = new WebSocket(wsUri)
    log('Connecting...')
    conn.onopen = function () {
      reconnect_time = reconnect_time_base
      log('Connected.')
      update_ui()
    }
    conn.onmessage = function (e) {
      const message = JSON.parse(e.data)
      if (!(message.user in colors)) {
        colors[message.user] = get_username_color()
      }

      if (message.message_type == 'Chat') {
        log(message.message, message.user, new Date(message.time), message.message_id)
      }
      else if (message.message_type == 'Delete') {
        const elem = document.querySelector(`#msg-${message.target_id}`)

        elem.innerHTML = "<em>deleted</em>"
        elem.parentElement.classList.add("deleted");
      }
      else if (message.message_type == 'Clear') {
        clear_all();
      }
      else if (message.message_type == 'Meta') {
        let elem = document.querySelector('#counter')
        let title_elem = document.querySelector('#title')

        elem.innerHTML = message.users
        title_elem.innerHTML = message.title

      }
      else if (message.message_type == 'Server') {
        //don't love undefined. Maybe pull it out as a constant? TODO
        log(message.message, undefined, new Date(message.time), message.message_id)
      }
      else {
        log("message of type" + message.message_type + "not accounted for.")
      }
      update_ui()
    }
    conn.onclose = function () {
      log('Disconnected.')
      conn = null
      setTimeout(connect, reconnect_time)
      reconnect_time *= reconnect_mult
      console.log("reconnect time  " + reconnect_time)
      update_ui()
    }

  }

  function update_ui() {
    const status = document.querySelector('#status')

    if (conn == null) {
      status.classList.remove('chat-connected')
      status.classList.add('chat-disconnected')
    } else {
      status.classList.remove('chat-disconnected')
      status.classList.add('chat-connected')
    }
  }

  function toggle_lights() {
    let body = document.getElementsByTagName('body')[0]
    let text = document.getElementById("text")
    let send = document.getElementById("send")
    let picker = document.getElementById("picker")

    let log = document.getElementById("log")
    const lights = document.getElementById("toggle-lights").checked

    let to_style = [ body, text, send, picker]
    for(let i = 0; i < to_style.length; i++) {
        const elem = to_style[i]
        elem.style.backgroundColor = lights ? LIGHT_BACKGROUND : DARK_BACKGROUND;
        elem.style.color = lights? LIGHT_TEXT : DARK_TEXT;
    }
    //body.setAttribute("style", lights ? `background-color: ${LIGHT_BACKGROUND}; color: ${LIGHT_TEXT}` : `background-color: ${DARK_BACKGROUND}; color: ${DARK_TEXT}`)
    log.setAttribute("style", lights ? `border-color: ${LIGHT_TEXT}` : `border-color: ${DARK_TEXT}`)
    log.classList.toggle("lights", lights );

    for (let key in colors) {
      colors[key] = get_username_color()
    }

    let usernames = document.getElementsByClassName("chat-username");

    for (let i = 0; i < usernames.length; i++) {
      const username = usernames.item(i);
      const name = username.innerText;
      username.setAttribute("style", `color: ${colors[name]}`)
    }
  }

  function toggle_bigmoji() {
    const bigmoji = document.getElementById("toggle-bigmoji").checked
    let op = enshrinken
    if(bigmoji)
      op = enbiggen
    let chatMessages = document.getElementsByClassName('chat-message-text')
    for (let i = 0; i < chatMessages.length; i++) {
      op(chatMessages[i])
    }
  }

  function toggle_small_stickers() {
    const small = document.getElementById('toggle-small-stickers').checked
    const chatcontainer= document.getElementById('chat')
    chatcontainer.classList.toggle("small-stickers", small );
  }

  observer = new IntersectionObserver(events => {
    chat_scrolled = (events[0].intersectionRatio <= 0)
    const pause = !document.getElementById('toggle-pause').checked
    const chatcontainer= document.getElementById('chat')
    if(chat_scrolled && pause)
        chatcontainer.classList.add('scroll-pause')
    else
        chatcontainer.classList.remove('scroll-pause')
    },
    { root: document.getElementById('log'),rootMargin: '0px',threshold: 0}
  )
  observer.observe(document.getElementById('log-pixel'))

  document.querySelector("#toggle-timestamps").onclick = function () {
    const chatlog = document.querySelector('#log')
    if (this.checked) {
      chatlog.classList.remove('hide-timestamps')
    } else {
      chatlog.classList.add('hide-timestamps')
    }
  }

  document.querySelector('#toggle-small-stickers').onclick = function() {
    toggle_small_stickers()
  }

  document.querySelector("#toggle-lights").onclick = function () {
    toggle_lights()
  }

  document.querySelector("#toggle-bigmoji").onclick = function () {
    toggle_bigmoji()
  }

  document.querySelector('#send').onclick = function () {
    const textElement = document.querySelector('#text')
    const text = textElement.value

    conn.send(text)
    textElement.value = ''
    textElement.focus()
    return false
  }

  document.querySelector('#text').onkeyup = function (e) {
    if (e.keyCode === 13) {
      document.querySelector('#send').click()
      return false
    }
    return false
  }

  document.querySelector('#scroll-status').onclick = function() {
    const chatlog = document.getElementById('log')
    chatlog.scrollTop = chatlog.scrollHeight
    return false
  }

  document.querySelector('#log').addEventListener('click', function(e) {
    if (!e.target.matches('a'))
      return

    e.preventDefault()
    e.stopPropagation()
    window.open(e.target.href, '_blank')
  })

  connect()
  toggle_lights()

  console.log("Loading sticker picker....")

  await setup_picker()
})()
