const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    chat: [
      path.resolve(__dirname, 'static', 'js', 'chat.js'),
      path.resolve(__dirname, 'static', 'js', 'emoji-picker.js'),
    ],
    profile: [
      path.resolve(__dirname, 'static', 'js', 'sticker-filesize.js'),
      path.resolve(__dirname, 'static', 'js', 'poster-filesize.js')
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'static', 'dist'),
  },
};
