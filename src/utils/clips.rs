use regex::Regex;
use std::io::{self, Write};
use std::{env, str};
use tokio::process::Command;

pub async fn clip_from_hls () -> Option<String> {
    let data_root = env::var("DATA_ROOT").unwrap();
    let now = chrono::Local::now().timestamp();

    let tmp_output = format!(
        "{}/clop-{}.mp4",
        &data_root,
        &now
    );
    let output = format!(
        "{}/clip-{}.mp4",
        &data_root,
        &now
    );

    info!("Writing clip to {}", &output);

    const CLIP_DURATION: u64 = 60; // TODO: make configurable?

    let segment_count: u64 = env::var("HLS_SEGMENT_COUNT")
        .expect("HLS_SEGMENT_COUNT is unset")
        .parse().unwrap();
    let segment_duration: u64 = env::var("HLS_SEGMENT_DURATION_SECONDS")
        .expect("HLS_SEGMENT_DURATION_SECONDS is unset")
        .parse().unwrap();

    let buffer = segment_count * segment_duration;
    let discard_start = buffer - CLIP_DURATION;

    info!("Discarding the first {} seconds of buffer out of a calculated total of {}", &discard_start, &buffer);

    let hls_manifest = env::var("HLS_MANIFEST").unwrap();

    match Command::new("ffmpeg")
        .arg("-allowed_extensions")
        .arg("ALL")
        .arg("-noaccurate_seek")
        .arg("-live_start_index")
        .arg("0")
        .arg("-ss")
        .arg(discard_start.to_string())
        .arg("-t")
        .arg(CLIP_DURATION.to_string())
        .arg("-i")
        .arg(&hls_manifest)
        .arg("-c")
        .arg("copy")
        .arg(&tmp_output)
        .output()
        .await {
            Ok(result) => {
                io::stdout().write_all(&result.stdout).unwrap();
                io::stderr().write_all(&result.stderr).unwrap();

                let start_pts = match Command::new("ffprobe")
                    .arg("-v")
                    .arg("error")
                    .arg("-select_streams")
                    .arg("v")
                    .arg("-show_entries")
                    .arg("stream=start_time")
                    .arg(&tmp_output)
                    .output()
                    .await {
                        Ok(result) => {
                            io::stdout().write_all(&result.stdout).unwrap();
                            io::stderr().write_all(&result.stderr).unwrap();

                            let stdout = str::from_utf8(&result.stdout).unwrap();
                            match Regex::new("start_time=([0-9\\.]*)").unwrap().captures(stdout) {
                                Some(caps) => String::from(&caps[1]),
                                None => String::from("0"),
                            }
                        },
                        Err(e) => String::from("0")
                    };

                debug!("ffprobe found {} seconds of junk at the start of the clip", &start_pts);

                // building a clip fron live HLS often results in some junk at
                // the beginning, so we chop off the first JUNK_POSSIBILITY
                // seconds
                match Command::new("ffmpeg")
                    .arg("-ss")
                    .arg(&start_pts)
                    .arg("-i")
                    .arg(&tmp_output)
                    .arg("-c")
                    .arg("copy")
                    .arg(&output)
                    .output()
                    .await {
                        Ok(result) => {
                            io::stdout().write_all(&result.stdout).unwrap();
                            io::stderr().write_all(&result.stderr).unwrap();

                            Some(output)
                        },
                        Err(e) => {
                            error!("Failed to create clip from HLS: {:?}", e);
                            None
                        }
                    }
            },
            Err(e) => {
                error!("Failed to create clip from HLS: {:?}", e);
                None
            }
        }
}
