use actix_http::HttpMessage;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::HttpRequest;
use argon2::{self, Config};
use diesel::prelude::*;
use rand::prelude::*;
use std::env;
use unic_ucd;
use uuid::Uuid;

use crate::models::*;
use crate::models;
use crate::schema::confirmations;
use crate::schema::password_resets;
use crate::schema::users::dsl::*;
use crate::schema::users;
use crate::utils::mail::*;
use crate::utils::session::*;

pub fn create_confirmation_for (
    email_addr: &str,
    connection: &mut PgConnection,
) -> Confirmation {
    let new_conf = Confirmation {
        email: email_addr.to_string(),
        expires_at: chrono::Utc::now().naive_utc() + chrono::Duration::hours(24),
        id: Uuid::new_v4().to_string(),
    };

    diesel::insert_into(confirmations::table)
        .values(&new_conf)
        .execute(connection)
        .unwrap();

    new_conf
}

pub fn create_reset_for (
    email_addr: &str,
    connection: &mut PgConnection,
) -> PasswordReset {
    let reset = PasswordReset {
        email: email_addr.to_string(),
        expires_at: chrono::Utc::now().naive_utc() + chrono::Duration::hours(1),
        id: Uuid::new_v4().to_string(),
    };

    diesel::insert_into(password_resets::table)
        .values(&reset)
        .execute(connection)
        .unwrap();

    reset
}

pub fn signup_existing_user (
    user: User,
    connection: &mut PgConnection,
    session: &Session,
) -> () {
    let smtp_enabled = match env::var("SMTP_ENABLED") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    if user.activated || !smtp_enabled {
        flash(
            &session,
            "error",
            "a user already exists with that email address"
        );
    } else {
        let new_confirmation = create_confirmation_for(
            &user.email,
            connection,
        );
        send_confirmation_email(new_confirmation);
        flash(
            &session,
            "warning",
            "user exists; re-sending confirmation email"
        );
    }
}

pub fn is_allowed_char (ch: &char) -> bool {
    if unic_ucd::common::is_white_space(*ch) { return false };
    if unic_ucd::category::GeneralCategory::of(*ch) == unic_ucd::category::GeneralCategory::PrivateUse {
        return false
    };

    // we're going to just strip nonprintable ASCII characters, to preserve ZWJ
    // etc
    if !ch.is_ascii() { return true };
    // now this is some sick shit huh
    //
    // https://dev.to/elasticrash/displaying-all-printable-utf-8-characters-using-rust-2788
    format!("{:?}", ch).len() < 7
}

pub fn signup_new_user (
    user_name: &str,
    email_addr: &str,
    password: &str,
    connection: &mut PgConnection,
    session: &Session,
    req: HttpRequest,
) -> () {
    match get_user_by_name(user_name, connection) {
        // db query success
        Some(_user) => {
            flash(
                &session,
                "error",
                "username already in use"
            );
        },
        None => create_or_login(
            user_name,
            email_addr,
            password,
            connection,
            session,
            req,
        ),
    }
}

pub fn create_or_login (
    user_name: &str,
    email_addr: &str,
    password: &str,
    connection: &mut PgConnection,
    session: &Session,
    req: HttpRequest,
) -> () {
    let smtp_enabled = match env::var("SMTP_ENABLED") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let new_user = create_user(
        user_name,
        email_addr,
        password,
        connection,
    );

    match new_user {
        Some(user) => {
            if smtp_enabled {
                let new_confirmation = create_confirmation_for(
                    &user.email,
                    connection,
                );
                send_confirmation_email(new_confirmation);
                flash(
                    &session,
                    "info",
                    "check your email to confirm your account"
                );
            } else {
                Identity::login(&req.extensions(), user.username.to_owned()).unwrap();
            }
        },
        None => panic!("Failed to create user"),
    }
}

pub fn create_user<'a> (
    name: &'a str,
    email_addr: &'a str,
    password: &'a str,
    connection: &mut PgConnection,
) -> Option<models::User> {
    let smtp_enabled = match env::var("SMTP_ENABLED") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let salt: u64 = random();
    let config = Config::original();

    let new_user = User {
        activated: !smtp_enabled,
        email: email_addr.to_owned(),
        id: Uuid::new_v4().to_string(),
        indefinitely_banned: false,
        last_namechange: chrono::Utc::now().naive_utc(),
        live: false,
        moderator: false,
        password_hash: argon2::hash_encoded(password.as_bytes(), &salt.to_be_bytes(), &config).unwrap(),
        stream_key: None,
        streamer: false,
        username: name.to_owned(),
        mjpeg: false,
    };

    let deez = diesel::insert_into(users::table)
        .values(&new_user)
        .execute(connection);

    match deez {
        Ok(_) => Some(new_user),
        Err(err) => {
            error!("Failed to create user: {}", err);
            None
        }
    }
}

pub fn get_user_by_email(email_addr: &str, connection: &mut PgConnection) -> Result<Option<models::User>, diesel::result::Error>{
    let user = users
                .filter(email.eq(email_addr))
                .first::<models::User>(connection)
                .optional()?;
    Ok(user)
}

pub fn get_user_by_name(
    name: &str,
    connection: &mut PgConnection
) -> Option<models::User> {
    let user_query = match users.load::<models::User>(connection) {
        Ok(u) => u,
        Err(e) => {
            panic!("Failed to query users: {}", e)
        },
    };

    let matches = user_query
        .iter()
        .cloned()
        .filter(|u| u.username.to_lowercase() == name.to_lowercase())
        .collect::<Vec<models::User>>();

    match matches.first() {
        Some(thing) => Some(thing.to_owned()),
        None => None,
    }
}

pub fn mod_user(name: &str, connection: &mut PgConnection)
    -> Result<(), diesel::result::Error>
{
    diesel::update(users.filter(username.eq(name)))
        .set(moderator.eq(true))
        .execute(connection)?;

    Ok(())
}

pub fn demod_user(name: &str, connection: &mut PgConnection)
    -> Result<(), diesel::result::Error>
{
    diesel::update(users.filter(username.eq(name)))
        .set(moderator.eq(false))
        .execute(connection)?;

    Ok(())
}

pub fn ban_user(name: &str, connection: &mut PgConnection)
    -> Result<(), diesel::result::Error>
{
    diesel::update(users.filter(username.eq(name)))
        .set(indefinitely_banned.eq(true))
        .execute(connection)?;

    Ok(())
}

pub fn unban_user(name: &str, connection: &mut PgConnection)
    -> Result<(), diesel::result::Error>
{
    diesel::update(users.filter(username.eq(name)))
        .set(indefinitely_banned.eq(false))
        .execute(connection)?;

    Ok(())
}

pub fn user_has_mod_powers(
    user: &User) -> bool {
    (user.moderator || user.streamer) && !user.indefinitely_banned
}

pub fn user_has_stream_powers(user: &User) -> bool {
    user.streamer && !user.indefinitely_banned
}


pub fn has_mod_powers(
    name: &str,
    connection: &mut PgConnection
) -> Result<Option<bool>, diesel::result::Error> {
    match get_user_by_name(name, connection) {
        Some(user) => Ok(Some(user_has_mod_powers(&user))),
        None => Ok(None)
    }
}

pub fn has_stream_powers(name: &str, connection: &mut PgConnection) ->Result<Option<bool>, diesel::result::Error> {
    match get_user_by_name(name, connection) {
        Some(user) => Ok(Some(user_has_stream_powers(&user))),
        None => Ok(None)
    }
}

pub fn is_banned(name: &str, connection: &mut PgConnection) ->Result<Option<bool>, diesel::result::Error> {
    match get_user_by_name(name, connection) {
        Some(user) => Ok(Some(user.indefinitely_banned)),
        None => Ok(None)
    }
}

pub fn who_is_live(connection: &mut PgConnection) -> Option<models::User> {
    users
        .filter(live.eq(true))
        .first::<models::User>(connection)
        .optional()
        .unwrap()
}
