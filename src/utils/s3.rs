use rusoto_core;
use rusoto_credential::EnvironmentProvider;
use rusoto_s3::{S3Client, S3, PutObjectRequest};
use std::fs::File;
use std::io::Read;
use std::env;

pub async fn make_s3_client (endpoint: String, region: String, provider: EnvironmentProvider) -> S3Client {
    let region = rusoto_core::Region::Custom {
        name: region,
        endpoint,
    };

    S3Client::new_with(rusoto_core::HttpClient::new().unwrap(), provider, region)
}

pub async fn make_s3_assets_client () -> S3Client {
    let s3_endpoint = env::var("S3_ASSETS_ENDPOINT").unwrap();
    let s3_region = env::var("S3_ASSETS_REGION").unwrap();

    let creds = EnvironmentProvider::with_prefix("S3_ASSETS");

    make_s3_client(s3_endpoint, s3_region, creds).await
}

pub async fn make_s3_segments_client () -> S3Client {
    let s3_endpoint = env::var("S3_CLIPS_ENDPOINT").unwrap();
    let s3_region = env::var("S3_CLIPS_REGION").unwrap();

    let creds = EnvironmentProvider::with_prefix("S3_CLIPS");

    make_s3_client(s3_endpoint, s3_region, creds).await
}

pub async fn upload_clip (filename: &str) -> Option<String> {
    let data_root = env::var("DATA_ROOT").unwrap();
    let clips_bucket = env::var("S3_CLIPS_BUCKET").unwrap();
    let clips_endpoint = env::var("S3_CLIPS_PUBLIC_ENDPOINT").unwrap();

    let mut file;

    match File::open(&filename) {
        Ok(handle) => {
            file = handle;
        },
        Err(err) => {
            error!("Failed to open {}: {}", &filename, err);
            return None;
        }
    }

    let mut buffer = Vec::new();
    match file.read_to_end(&mut buffer) {
        Ok(_) => {
            debug!("Read {} into buffer", &filename)
        },
        Err(err) => {
            error!("Failed reading {} in to buffer: {}", &filename, err);
            return None;
        }
    }

    let basename = str::replace(&filename, &format!("{}/", &data_root), "");

    let client = make_s3_segments_client().await;
    match client.put_object(PutObjectRequest {
        acl: Some("public-read".to_string()),
        body: Some(buffer.into()),
        bucket: clips_bucket.to_owned(),
        key: basename.to_owned(),
        ..Default::default()
    }).await {
        Ok(_) => {
            Some(format!("{}/{}/{}", &clips_endpoint, &clips_bucket, &basename))
        },
        Err(e) => {
            error!("Failed to upload clip: {:?}", e);
            None
        }
    }
}
