use diesel::PgConnection;
use diesel::prelude::*;
use regex::Regex;
use rusoto_core;
use rusoto_s3::{S3, S3Client, PutObjectRequest};
use serde_json;
use std::collections::HashMap;
use std::env;

use crate::models;
use crate::schema::stickers::dsl::*;

pub const STICKER_MIMES: [&str; 6] = [
    "image/apng",
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/svg+xml",
    "image/webp",
];

pub fn make_allowed_shortcode (shortcode: &str) -> String {
    let reggie = Regex::new("[^a-zA-Z0-9]").unwrap();

    reggie.replace_all(shortcode, "").to_string()
}

pub fn all_stickers(conn: &mut PgConnection) -> Vec<models::Sticker> {
    return match stickers.load::<models::Sticker>(conn) {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to load stickers: {:?}", err);
            Vec::new()
        }
    };
}

pub fn stickers_by_packs(conn: &mut PgConnection) -> HashMap::<String, Vec<models::Sticker>> {
    let stickies = all_stickers(conn);

    let sticker_packs = match stickers.select(pack).distinct().load::<String>(conn) {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to load stickers: {:?}", err);
            Vec::new()
        }
    };

    let mut sorted_stickers = HashMap::<String, Vec<models::Sticker>>::new();

    for s in sticker_packs {
        let sticks = stickies.iter().filter(|stick| stick.pack == s).map(|s| s.to_owned()).collect();
        sorted_stickers.insert(s, sticks);
    }

    sorted_stickers
}

pub fn make_stickers_json(conn: &mut PgConnection) -> String {
    let sorted_stickers = stickers_by_packs(conn);

    match serde_json::to_string(&sorted_stickers) {
        Ok(json) => json,
        Err(err) => panic!("Failed to serialize JSON: {:?}", err),
    }
}

// pub fn delete_sticker_fs(sticker_id: &str) -> () {
//     let data_root = match env::var("DATA_ROOT") {
//         Ok(val) => val,
//         Err(_) => String::from("/data"),
//     };

//     let sticker_path = format!("{}/stickers/{}", data_root, sticker_id);
//     match fs::remove_file(&sticker_path) {
//         Ok(_) => debug!("Deleted {}", &sticker_path),
//         Err(err) => error!("Failed to delete {}: {:?}", &sticker_path, err),
//     };
// }

pub async fn write_stickers_json(conn: &mut PgConnection, client: S3Client) -> () {
    let stickers_json = make_stickers_json(conn);
    let bips = rusoto_core::ByteStream::from(stickers_json.into_bytes());

    let assets_bucket = env::var("S3_ASSETS_BUCKET").unwrap();
    client.put_object(PutObjectRequest {
        acl: Some("public-read".to_string()),
        body: Some(bips),
        bucket: assets_bucket,
        key: "stickers/stickers.json".to_string(),
        ..Default::default()
    }).await.unwrap();
}
