use serde::Serialize;
use serde::Deserialize;
use std::env;

#[derive(Serialize, Deserialize)]
pub enum State {
#[serde(rename = "play")]
    Play,
#[serde(rename = "pause")]
    Pause,
#[serde(rename = "stop")]
    Stop,
}
#[derive(Serialize, Deserialize)]
pub struct MjpegControl {
    state: State,
}

pub async fn set_state(state : State ) -> () {
    let mjpeg_control_location = match env::var("MJPEG_CONTROL_LOCATION") {
        Ok(val) => val,
        Err(_) => String::from("/mjpeg_control"),
    };

    match env::var("MJPEG_HOST") {
        Ok(val) => {
            let client = reqwest::Client::new();
            let mjpeg_control_address = format!( "{}{}",val, mjpeg_control_location);
            let _ = client.post(&mjpeg_control_address)
                            .json(&MjpegControl{ state: state})
                            .send().await;
        }
        Err(_) => {}
    };
}
pub async fn get_state() -> reqwest::Result<State> {
    let mjpeg_control_location = match env::var("MJPEG_CONTROL_LOCATION") {
        Ok(val) => val,
        Err(_) => String::from("/mjpeg_control"),
    };

    match env::var("MJPEG_HOST") {
        Ok(val) => {
            let client = reqwest::Client::new();
            let mjpeg_control_address = format!( "{}{}",val, mjpeg_control_location);
            let res = client.get(&mjpeg_control_address)
                            .send()
                            .await?
                            .json::<MjpegControl>()
                            .await?;
            Ok(res.state)
        }
        Err(_) => Ok(State::Stop)
    }
}
