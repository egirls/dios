use actix_http::header;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::{web, get, HttpRequest, HttpResponse};
use diesel::prelude::*;
use std::env;

use crate::ConnectionPool;
use crate::models;
use crate::routes::util::*;
use crate::schema::server_about;
use crate::templates;
use crate::utils;
use crate::utils::user::*;

#[get("/profile")]
pub async fn profile (
    _req: HttpRequest,
    identity: Result<Identity, actix_web::Error>,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    match identity {
        Ok(extracted_id) => {
            if let Ok(iden) = extracted_id.id() {
                let mut connection = pool.get().expect("couldn't get db connection from pool");

                if let Some(found_user) = get_user_by_name(&iden, &mut connection) {
                    let mut context = make_context(&session);
                    context.insert("user", &iden);
                    context.insert("email", &found_user.email);
                    context.insert("page_title", &format!("profile for {}", &iden));

                    if user_has_stream_powers(&found_user) {
                        if let Ok(server_about) = server_about::table.find("1").first::<models::ServerAbout>(&mut connection) {
                            context.insert("welcome", &server_about.welcome_message);
                            context.insert("blurb", &server_about.blurb);
                        } else {
                            context.insert("welcome", "hello and welcome");
                        }

                        let mjpeg_on = match env::var("MJPEG_ENABLED") {
                            Ok(val) => val == "true" || val == "1" ,
                            Err(_) => false,
                        };

                        context.insert("streamer", &true);
                        context.insert("mjpeg", &found_user.mjpeg);
                        context.insert("stream_key", &found_user.stream_key);
                        context.insert("mjpeg_enabled", &mjpeg_on);
                    }
                    if user_has_mod_powers(&found_user) {
                        let sorted_stickers = utils::stickers::stickers_by_packs(&mut connection);
                        context.insert("stickers", &sorted_stickers);
                        context.insert("moderator", &true);
                    }
                    return HttpResponse::Ok().body(templates::render("profile.html.tera", &context))
                }
            }
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        },
        Err(e) => {
            error!("{:?}", e);
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        }
    }
}
