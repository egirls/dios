use actix_http::header;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::{web, post, HttpMessage, HttpRequest, HttpResponse};
use diesel::prelude::*;
use serde::Deserialize;
use uuid::Uuid;

use crate::ConnectionPool;
use crate::routes::util::*;
use crate::schema::users::dsl::*;
use crate::templates;
use crate::utils::session::*;
use crate::utils::user::*;
use crate::mjpeg_client;

#[derive(Deserialize)]
pub struct NewUsernameForm<>{
    username: String,
}

#[derive(Deserialize)]
pub struct NewEmailForm<>{
    email: String,
}

#[derive(Deserialize)]
pub struct MjpegEnableForm<>{
    mjpeg: String,
}

#[post("/change_username")]
pub async fn change_username (
    req: HttpRequest,
    form: web::Form<NewUsernameForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        let newname: String = form.username
            .chars()
            .filter(|c| is_allowed_char(c) )
            .collect();

        let mut context = make_context(&session);
        context.insert("user", &iden);
        context.insert("page_title", &format!("profile for {}", &iden));

        match get_user_by_name(&iden, &mut connection) {
            Some(u) => {
                let now = chrono::Utc::now().naive_utc();

                if u.last_namechange + chrono::Duration::minutes(5) > now {
                    flash(
                        &session,
                        "error",
                        "you can only change your username once every five minutes",
                    );

                    return HttpResponse::SeeOther()
                        .insert_header((header::LOCATION, "/profile"))
                        .finish()
                } else {
                    match get_user_by_name(&newname, &mut connection) {
                        Some(_collision) => {
                            flash(
                                &session,
                                "error",
                                &format!("the name {} is already in use", &newname)
                            );

                            return HttpResponse::SeeOther()
                                .insert_header((header::LOCATION, "/profile"))
                                .finish()
                        },
                        None => {
                            diesel::update(users.filter(username.eq(&iden)))
                                .set((
                                    username.eq(&newname),
                                    last_namechange.eq(&now),
                                ))
                                .execute(&mut connection)
                                .unwrap();

                            Identity::login(&req.extensions(), String::from(&newname)).unwrap();

                            flash(
                                &session,
                                "info",
                                &format!("name updated successfully. hello, {}", &newname)
                            );

                            return HttpResponse::SeeOther()
                                .insert_header((header::LOCATION, "/"))
                                .finish()
                        },
                    }
                }
            },
            None => {
                flash(
                    &session,
                    "error",
                    &format!("failed to retrieve entry for '{}'; please contact an administrator", newname)
                );

                return HttpResponse::SeeOther()
                    .body(templates::render("profile.html.tera", &context))
            },
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}

#[post("/change_email")]
pub async fn change_email (
    _req: HttpRequest,
    form: web::Form<NewEmailForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        let newmail = &form.email;

        let mut context = make_context(&session);
        context.insert("user", &iden);
        context.insert("page_title", &format!("profile for {}", &iden));

        match get_user_by_name(&iden, &mut connection) {
            Some(_u) => {
                diesel::update(users.filter(username.eq(&iden)))
                    .set(email.eq(&newmail))
                    .execute(&mut connection)
                    .unwrap();

                flash(
                    &session,
                    "info",
                    &format!("email set to {}", &newmail)
                );

                return HttpResponse::SeeOther()
                    .insert_header((header::LOCATION, "/profile"))
                    .finish()
            },
            None => {
                flash(
                    &session,
                    "error",
                    &format!("failed to retrieve entry for '{}'; please contact an administrator", &iden)
                );

                return HttpResponse::SeeOther()
                    .body(templates::render("profile.html.tera", &context))
            },
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}

#[post("/change_stream_key")]
pub async fn change_stream_key (
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |u, conn| {
        if u.live {
            flash(
                &session,
                "error",
                &format!("you can't change your key while you're streaming")
            );

            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/profile"))
                .finish()
        }

        let new_key = Uuid::new_v4().to_string();

        diesel::update(u)
            .set(stream_key.eq(&new_key))
            .execute(conn)
            .unwrap();

        HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/profile"))
            .finish()
    })
}

#[post("/change_mjpeg")]
pub async fn change_mjpeg (
    form: web::Form<MjpegEnableForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let int_mjpeg : i32 = form.mjpeg.parse().unwrap();
    let new_mjpeg : bool = int_mjpeg.ne(&0);
    let session_ref = & session;

    as_streamer_async(identity, pool, |u, mut conn| async move {
        if u.live {
            if new_mjpeg {
                mjpeg_client::set_state(mjpeg_client::State::Play).await;
            } else {
                mjpeg_client::set_state(mjpeg_client::State::Pause).await;
            }
        }
        diesel::update(&u)
            .set(mjpeg.eq(&new_mjpeg))
            .execute(&mut conn)
            .unwrap();

        flash(
            session_ref,
            "info",
            &format!("mjpeg streaming enable changed, {}", &new_mjpeg)
        );

        HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/profile"))
            .finish()
    }).await
}
