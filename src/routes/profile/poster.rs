use actix_http::header;
use actix_identity::Identity;
use actix_multipart::Multipart;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use futures::{StreamExt, TryStreamExt};
use rusoto_core;
use rusoto_s3::{S3, PutObjectRequest};
use std::env;

use crate::ConnectionPool;
use crate::utils;
use crate::utils::session::*;
use crate::utils::user::*;

#[post("/upload_poster")]
pub async fn upload_poster(
    mut payload: Multipart,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &mut connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .insert_header((header::LOCATION, "/"))
                    .finish()
            }

            while let Ok(Some(mut field)) = payload.try_next().await {
                let mime = field.content_type().unwrap().essence_str();
                if !utils::stickers::STICKER_MIMES.iter().any(|&allowed| allowed == mime) {
                    flash(
                        &session,
                        "error",
                        "sorry, that's not a supported image format"
                    );

                    return HttpResponse::SeeOther()
                        .insert_header((header::LOCATION, "/profile"))
                        .finish()
                }

                let mut filesize: usize = 0;
                let mut data: Vec<actix_web::web::Bytes> = Vec::new();

                while let Some(chunk) = field.next().await {
                    let blob = chunk.unwrap();
                    filesize += &blob.len();

                    if filesize > 1000000 {
                        flash(
                            &session,
                            "error",
                            "the image is too large"
                        );

                        return HttpResponse::SeeOther()
                            .insert_header((header::LOCATION, "/profile"))
                            .finish()
                    }
                    data.push(blob);
                }
                let bonk = data.iter().flat_map(|b| b.as_ref().to_owned()).collect::<Vec<u8>>();
                let bips = rusoto_core::ByteStream::from(bonk);

                let client = utils::s3::make_s3_assets_client().await;

                let assets_bucket = env::var("S3_ASSETS_BUCKET").unwrap();
                client.put_object(PutObjectRequest {
                    acl: Some("public-read".to_string()),
                    body: Some(bips),
                    bucket: assets_bucket,
                    key: "images/poster".to_string(),
                    ..Default::default()
                }).await.unwrap();

            }
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/profile"))
                .finish()
        } else {
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}
