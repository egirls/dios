use actix_http::header;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use diesel::prelude::*;
use serde::Deserialize;

use crate::ConnectionPool;
use crate::schema::server_about;
use crate::routes::util::*;

#[derive(Deserialize)]
pub struct UpdateServerInfoForm<>{
    welcome_message: Option<String>,
    blurb: Option<String>,
}

#[post("/update_server_info")]
pub async fn update_server_info (
    form: web::Form<UpdateServerInfoForm>,
    identity: Identity,
    _session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |_user, conn| {
        diesel::update(server_about::table.find("1"))
            .set((
                server_about::welcome_message.eq(&form.welcome_message),
                server_about::blurb.eq(&form.blurb),
            ))
            .execute(conn)
            .unwrap();

        HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/profile"))
            .finish()
    })
}
