use actix_http::header;
use actix_identity::Identity;
use actix_multipart::Multipart;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use diesel::prelude::*;
use futures::{StreamExt, TryStreamExt};
use rusoto_core;
use rusoto_s3::{S3, PutObjectRequest};
use serde::Deserialize;
use std::env;
use std::str;
use uuid::Uuid;

use crate::ConnectionPool;
use crate::models;
use crate::routes::util::*;
use crate::schema::stickers;
use crate::schema::stickers::dsl::*;
use crate::utils;
use crate::utils::session::*;

#[derive(Deserialize)]
pub struct DeleteStickerForm<>{
    sticker_id: String,
}

#[derive(Deserialize)]
pub struct RenameStickerForm<>{
    sticker_id: String,
    new_name: String,
}

#[post("/add_sticker")]
pub async fn add_sticker(
    mut payload: Multipart,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = utils::user::get_user_by_name(&iden, &mut connection) {
            if !utils::user::user_has_mod_powers(&u) {
                return HttpResponse::Forbidden()
                    .insert_header((header::LOCATION, "/"))
                    .finish()
            }

            let mut sticker_data: Vec<actix_web::web::Bytes> = Vec::new();
            let mut sticker_pack_b: Vec<actix_web::web::Bytes> = Vec::new();
            let mut sticker_name_b: Vec<actix_web::web::Bytes> = Vec::new();

            // use the permanent id as the filename to avoid having to write
            // to the fs during renames
            let sticker_id = Uuid::new_v4().to_string();

            while let Ok(Some(mut field)) = payload.try_next().await {
                let field_name = field.content_disposition().get_name().unwrap();

                match field_name {
                    // the actual file data
                    "sticker" => {
                        let mime = field.content_type().unwrap().essence_str();

                        if !utils::stickers::STICKER_MIMES.iter().any(|&allowed| allowed == mime) {
                            flash(
                                &session,
                                "error",
                                "sorry, that's not a supported image format"
                            );

                            return HttpResponse::SeeOther()
                                .insert_header((header::LOCATION, "/profile"))
                                .finish()
                        }

                        let mut filesize: usize = 0;
                        while let Some(chunk) = field.next().await {
                            let blob = chunk.unwrap();
                            filesize += &blob.len();

                            if filesize > 250000 {
                                flash(
                                    &session,
                                    "error",
                                    "the sticker is too large (> 250kb)"
                                );

                                return HttpResponse::SeeOther()
                                    .insert_header((header::LOCATION, "/profile"))
                                    .finish()
                            }
                            sticker_data.push(blob);
                        }

                    },
                    "sticker_pack" => {
                        while let Some(chunk) = field.next().await {
                            let blob = chunk.unwrap();
                            sticker_pack_b.push(blob);
                        }
                    },
                    "sticker_name" => {
                        while let Some(chunk) = field.next().await {
                            let blob = chunk.unwrap();
                            sticker_name_b.push(blob);
                        }
                    },
                    _ => {
                    },
                };

            }

            let sticker_pack_vec: Vec<u8> = sticker_pack_b
                .iter()
                .flat_map(|v| v.iter().map(|b| b.to_owned() ) )
                .collect();
            let sticker_pack = String::from_utf8(sticker_pack_vec).unwrap();

            let sticker_name_vec: Vec<u8> = sticker_name_b
                .iter()
                .flat_map(|v| v.iter().map(|b| b.to_owned() ) )
                .collect();
            let sticker_name = String::from_utf8(sticker_name_vec).unwrap();

            debug!("pack: {:?}", sticker_pack);
            debug!("name: {:?}", sticker_name);

            match diesel::select(diesel::dsl::exists(stickers.filter(
                pack.eq(&sticker_pack).and(name.eq(&sticker_name))
            ))).get_result::<bool>(&mut connection) {
                Ok(result) => {
                    if result == true {
                        flash(
                            &session,
                            "error",
                            "that sticker name is already in use"
                        );
                        return HttpResponse::SeeOther()
                            .insert_header((header::LOCATION, "/profile"))
                            .finish()
                    }
                },
                Err(err) => {
                    error!("Failed while querying the DB: {:?}", err);
                    flash(
                        &session,
                        "error",
                        "sorry something exploded"
                    );
                    return HttpResponse::SeeOther()
                        .insert_header((header::LOCATION, "/profile"))
                        .finish()
                }
            };

            let bonk = sticker_data.iter().flat_map(|b| b.as_ref().to_owned()).collect::<Vec<u8>>();
            let bips = rusoto_core::ByteStream::from(bonk);

            let client = utils::s3::make_s3_assets_client().await;
            let assets_bucket = env::var("S3_ASSETS_BUCKET").unwrap();

            client.put_object(PutObjectRequest {
                acl: Some("public-read".to_string()),
                body: Some(bips),
                bucket: assets_bucket,
                key: format!("stickers/{}", &sticker_id),
                ..Default::default()
            }).await.unwrap();

            let new_sticker = models::Sticker {
                id: sticker_id,
                pack: sticker_pack,
                name: sticker_name,
            };

            let inserts = diesel::insert_into(stickers::table)
                .values(&new_sticker)
                .execute(&mut connection);

            match inserts {
                Ok(_) => {
                    utils::stickers::write_stickers_json(&mut connection, client).await;

                    flash(
                        &session,
                        "info",
                        "sticker added successfully",
                    )
                },
                Err(err) => {
                    error!("Failed to create sticker: {}", err);
                    flash(
                        &session,
                        "error",
                        "the sticker was too strong to be added, please speak with a higher power"
                    )
                }
            }

            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/profile"))
                .finish()
        } else {
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}

#[post("/rename_sticker")]
pub async fn rename_sticker (
    form: web::Form<RenameStickerForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let session_ref = & session;
    let sticker_id = &form.sticker_id;

    let new_parts: Vec<&str> = form.new_name.split("/").collect();
    let new_pack = &new_parts[0].to_string();
    let new_name = &new_parts[1].to_string();

    as_streamer_async(identity, pool, |_user, mut connection| async move {
        match diesel::update(stickers.find(sticker_id))
                    .set((
                        stickers::pack.eq(new_pack),
                        stickers::name.eq(new_name),
                    )).execute(&mut connection) {
                        Ok(_) => {
                            let client = utils::s3::make_s3_assets_client().await;
                            utils::stickers::write_stickers_json(&mut connection, client).await;

                            flash(
                                session_ref,
                                "info",
                                "sticker renamed successfully"
                            );
                        },
                        Err(e) => {
                            warn!("could not rename sticker: {:?} ", e);
                            flash(
                                session_ref,
                                "error",
                                "the sticker was too strong to be renamed, please speak with a higher power"
                            );
                        }
                    }

        HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/profile"))
            .finish()
    }).await
}

#[post("/delete_sticker")]
pub async fn delete_sticker (
    form: web::Form<DeleteStickerForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let session_ref = & session;
    let sticker_id = &form.sticker_id;

    as_streamer_async(identity, pool, |_user, mut connection| async move {
        match diesel::delete(stickers.find(sticker_id)).execute(&mut connection) {
            Ok(_) => {
                let client = utils::s3::make_s3_assets_client().await;
                utils::stickers::write_stickers_json(&mut connection, client).await;

                flash(
                    session_ref,
                    "info",
                    "farewell, sweet sticky"
                )
            },
            Err(err) => {
                error!("Failed to delete sticker: {:?}", err);
                flash(
                    session_ref,
                    "error",
                    "the sticker was too powerful to delete, please speak with a higher power"
                )
            }
        };

        HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/profile"))
            .finish()
    }).await
}
