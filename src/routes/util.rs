use actix_http::header;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::{web, HttpResponse};
use diesel::prelude::*;
use std::env;
use tera::Context;

use crate::ConnectionPool;
use crate::PooledConnection;
use crate::models;
use crate::schema::server_about;
use crate::stream_info::StreamInfo;
use crate::utils::markdown;
use crate::utils::user::*;
use std::future::Future;


fn get_page_title() -> String {
    let title_env = "APP_TITLE";
    let page_title = match env::var(title_env) {
        Ok(val) => val,
        Err(_) => String::from("DIOS"),
    };

    page_title
}

pub fn make_context(session: &Session) -> tera::Context {
    let mut context = Context::new();
    context = merge_session_into_context(&session, context);
    context.insert("page_title", &get_page_title());

    context.insert("app_domain", &env::var("APP_DOMAIN").expect("APP_DOMAIN is unset"));
    context.insert("hls_app", &env::var("OME_APP_NAME").expect("OME_APP_NAME is unset"));
    context.insert("ingest_port", &env::var("OME_RTMP_PROV_PORT").expect("OME_RTMP_PROV_PORT is unset"));
    context.insert("hls_stream", &env::var("OME_STREAM_NAME").expect("OME_STREAM_NAME is unset"));

    let protocol = match env::var("HTTPS") {
        Ok(val) => {
            if val == "" || val == "false" {
                "http"
            } else {
                "https"
            }
        },
        Err(_) => "http",
    };
    context.insert("protocol", &protocol);

    context
}

pub fn add_stream_info_to_context(
    stream_info: web::Data<StreamInfo>,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    let empty = String::from("");
    let title_op = stream_info.title.read().unwrap();
    let title = title_op.as_ref().unwrap_or(&empty);
    ctx.insert("title", &title);

    ctx
}

pub fn add_server_info_to_context(
    connection: &mut PgConnection,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    if let Ok(server_about) = server_about::table.find("1").first::<models::ServerAbout>(connection) {
        let rendered_blurb = match &server_about.blurb {
            Some(blub) => Some(markdown::parse_inline(&blub)),
            None => None,
        };

        ctx.insert("welcome", &server_about.welcome_message);
        ctx.insert("blurb", &rendered_blurb);
    } else {
        ctx.insert("welcome", "hello and welcome");
    }

    ctx
}

pub fn add_user_info_to_context(
    user: Option<Identity>,
    connection: &mut PgConnection,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    if let Some(user) = user {
        let id = &user.id().unwrap();

        ctx.insert("user", id);

        if let Some(found_user) = get_user_by_name(id, connection) {
            if user_has_stream_powers(&found_user) {
                ctx.insert("user_is_streamer", &true);
                ctx.insert("stream_key", &found_user.stream_key);
            }
        }
    }

    if let Some(live_streamer) = who_is_live(connection) {
        ctx.insert("currently_streaming", &live_streamer.username);
    }

    ctx
}

pub fn everything_bagel(
    stream_info: web::Data<StreamInfo>,
    user: Option<Identity>,
    connection: &mut PgConnection,
    session: Session,
) -> tera::Context {
    let mut context = make_context(&session);
    context = add_stream_info_to_context(stream_info, context);
    context = add_server_info_to_context(connection, context);
    context = add_user_info_to_context(user, connection, context);

    context
}

pub fn merge_session_into_context(
    session: &Session,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    let alerts = vec![
        "info",
        "warning",
        "error",
    ];

    alerts.iter().for_each( |&key| ctx.insert(key, &get_vec_from_session(key, session)));

    ctx
}

fn get_vec_from_session(
    key: &str,
    session: &Session
) -> Vec<String> {
    let sesh = match session.get(key) {
        Ok(sesh) => {
            match sesh {
                Some(val) => val,
                None => Vec::new(),
            }
        },
        Err(_) => Vec::new()
    };

    session.remove(key);

    sesh
}

pub fn as_streamer<F>(
    identity: Identity,
    pool: web::Data<ConnectionPool>,
    block: F,
) -> HttpResponse
where F: Fn(&models::User, &mut PgConnection) -> HttpResponse {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &mut connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .insert_header((header::LOCATION, "/"))
                    .finish()
            }

            block(&u, &mut connection)
        } else {
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}
pub async fn as_streamer_async<'a, F, H>(
    identity: Identity,
    pool: web::Data<ConnectionPool>,
    block: F ,
) -> HttpResponse
where F: Fn(models::User, PooledConnection) -> H,
      H: Future<Output = HttpResponse>  + 'a {
    if let Ok(iden) = identity.id() {
        let mut connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &mut connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .insert_header((header::LOCATION, "/"))
                    .finish()
            }
            block(u, connection)
                .await
        } else {
            return HttpResponse::SeeOther()
                .insert_header((header::LOCATION, "/"))
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .insert_header((header::LOCATION, "/"))
            .finish()
    }
}
