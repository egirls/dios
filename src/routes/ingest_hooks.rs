use actix_web::{web, post, HttpResponse};
use diesel::prelude::*;
use regex::Regex;
use serde::Deserialize;
use serde::Serialize;

use crate::ConnectionPool;
use crate::models::*;
use crate::schema::users::dsl::*;
use crate::utils::user::user_has_stream_powers;
use crate::mjpeg_client;

#[derive(Deserialize,Debug)]
pub struct StreamRequest {
    direction: String,
    protocol: String,
    status: String,
    time: String,
    url: String,
}

#[derive(Deserialize,Debug)]
pub struct StreamPayload {
    request: StreamRequest,
}

#[derive(Serialize)]
pub struct StreamResponse {
    allowed: bool,
    lifetime: String,
    reason: String,
}

// https://airensoft.gitbook.io/ovenmediaengine/access-control/admission-webhooks#format
#[post("/on_connect")]
pub async fn on_connect (
    stream_payload: web::Json<StreamPayload>,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let mut connection = pool.get().expect("couldn't get db connection from pool");

    debug!("Got payload from ingest server: {:?}", &stream_payload);

    if &stream_payload.request.protocol == "LLHLS" {
        let response = StreamResponse {
            allowed: true,
            lifetime: String::from("0"),
            reason: String::from("sure watch my stream who cares"),
        };
        return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
    }

    if &stream_payload.request.protocol != "RTMP" {
        let response = StreamResponse {
            allowed: false,
            lifetime: String::from("0"),
            reason: String::from("not implemented"),
        };
        return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
    }

    let stream_url = &stream_payload.request.url;

    // we have to use stupid names because diesel pollutes the namespace with
    // its dsl
    let stream_key_VEVO: String;

    match Regex::new("\\?key=(.*)").unwrap().captures(stream_url) {
        Some(caps) => stream_key_VEVO = String::from(&caps[1]),
        None => {
            let response = StreamResponse {
                allowed: false,
                lifetime: String::from("0"),
                reason: String::from("malformed stream URL"),
            };
            return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
        }
    }

    debug!("Checking if stream key {} is valid", stream_key_VEVO);

    let new_streamer = users
        .filter(stream_key.eq(&stream_key_VEVO))
        .first::<User>(&mut connection)
        .optional()
        .unwrap();

    if &stream_payload.request.status == "opening" {
        if let Some(found_user) = new_streamer {
            if !user_has_stream_powers(&found_user) {
                let response = StreamResponse {
                    allowed: false,
                    lifetime: String::from("0"),
                    reason: format!("{:?} is not authorized to stream", &found_user),
                };
                return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
            }

            let already_live = users
                .filter(live.eq(true))
                .first::<User>(&mut connection)
                .optional()
                .unwrap();

            if let Some(og) = already_live {
                if og.email != found_user.email {
                    let response = StreamResponse {
                        allowed: false,
                        lifetime: String::from("0"),
                        reason: String::from("another user is already streaming"),
                    };
                    return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
                }
            }

            debug!("Authorized {} to stream", &found_user.username);

            if found_user.mjpeg {
                mjpeg_client::set_state(mjpeg_client::State::Play).await;
            } else {
                mjpeg_client::set_state(mjpeg_client::State::Pause).await;
            }
            diesel::update(&found_user)
                .set(live.eq(true))
                .execute(&mut connection)
                .unwrap();

            let response = StreamResponse {
                allowed: true,
                lifetime: String::from("0"),
                reason: String::from("let's go apeshitt"),
            };
            return HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
        }

    } else if &stream_payload.request.status == "closing"{
        if let Some(found_user) = new_streamer {
            debug!("{} going offline", &found_user.username);

            diesel::update(&found_user)
                .set(live.eq(false))
                .execute(&mut connection)
                .unwrap();
            mjpeg_client::set_state(mjpeg_client::State::Pause).await;
        }
        // https://airensoft.gitbook.io/ovenmediaengine/access-control/admission-webhooks#response-for-closing-status
        return HttpResponse::Ok().body("{}")
    }

    let response = StreamResponse {
        allowed: false,
        lifetime: String::from("0"),
        reason: String::from("an unknown error occurred"),
    };
    HttpResponse::Ok().body(serde_json::to_string(&response).unwrap())
}
