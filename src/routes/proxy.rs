use actix_http::header;
use actix_web::{get, HttpResponse, HttpRequest};
use std::env;

#[get("/images/{i}")]
pub async fn images(req: HttpRequest) -> HttpResponse {
    let img = req.match_info().get("i").unwrap();

    let s3_public = env::var("S3_ASSETS_PUBLIC_ENDPOINT").unwrap();
    let assets_bucket = env::var("S3_ASSETS_BUCKET").unwrap();

    return HttpResponse::TemporaryRedirect()
        .insert_header((header::LOCATION, format!("{}/{}/images/{}", &s3_public, &assets_bucket, &img)))
        .finish()
}

#[get("/stickers/{s}")]
pub async fn stickers(req: HttpRequest) -> HttpResponse {
    let sticky = req.match_info().get("s").unwrap();

    let s3_public = env::var("S3_ASSETS_PUBLIC_ENDPOINT").unwrap();
    let assets_bucket = env::var("S3_ASSETS_BUCKET").unwrap();

    return HttpResponse::TemporaryRedirect()
        .insert_header((header::LOCATION, format!("{}/{}/stickers/{}", &s3_public, &assets_bucket, &sticky)))
        .finish()
}
