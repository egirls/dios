#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;
extern crate actix_files;
extern crate argon2;
extern crate dotenv;
extern crate subprocess;
extern crate htmlescape;

use actix::*;
use actix_files::Files;
use actix_identity::IdentityMiddleware;
use actix_session::{SessionMiddleware, storage::CookieSessionStore};
use actix_web::{cookie::Key, web, middleware, App, HttpServer};
use dotenv::dotenv;
use env_logger;
use listenfd::ListenFd;
use std::env;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

pub mod utils {
    pub mod clips;
    pub mod mail;
    pub mod markdown;
    pub mod s3;
    pub mod session;
    pub mod stickers;
    pub mod user;
}

pub mod messages;
pub mod models;
pub mod schema;
pub mod stream_info;
pub mod templates;
pub mod mjpeg_client;

pub mod chat {
    pub mod server;
    pub mod sessions;
}

pub mod routes {
    pub mod auth;
    pub mod ingest_hooks;
    pub mod public;
    pub mod reset;
    pub mod util;
    pub mod proxy;

    pub mod profile {
        pub mod poster;
        pub mod profile;
        pub mod server_settings;
        pub mod stickers;
        pub mod user_settings;
    }
}

type ConnectionPool = r2d2::Pool<ConnectionManager<PgConnection>>;
type PooledConnection = r2d2::PooledConnection<ConnectionManager<PgConnection> >;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info")
    ).init();

    // HOST and PORT set the bind address
    let host_env = "HOST";
    let host = match env::var(host_env) {
        Ok(val) => val,
        Err(_) => String::from("localhost"),
    };
    let port_env = "PORT";
    let port = match env::var(port_env) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };
    let bind_address = format!("{}:{}", host, port);

    // for whether or not to use secure cookies
    let is_https = match env::var("HTTPS") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let pg_db = env::var("POSTGRES_DB").expect("POSTGRES_DB unset");
    let pg_host = env::var("POSTGRES_HOST").expect("POSTGRES_HOST unset");
    let pg_pass = env::var("POSTGRES_PASSWORD").expect("POSTGRES_PASSWORD unset");
    let pg_user = env::var("POSTGRES_USER").expect("POSTGRES_USER unset");
    let database_url = format!("postgres://{}:{}@{}/{}",
                               &pg_user,
                               &pg_pass,
                               &pg_host,
                               &pg_db,);

    let manager = ConnectionManager::<PgConnection>::new(&database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    let mut conn = pool.get().expect("couldn't get db connection from pool");
    conn.run_pending_migrations(MIGRATIONS).expect("Failed to run migrations");

    let server_state = web::Data::new(stream_info::StreamInfo::default());

    //for now, use a separate pool for the ChatServer
    let server = chat::server::ChatServer::new(server_state.clone(), pool.clone()).start();

    let mut server = HttpServer::new(move || {
        let session_mw =
            SessionMiddleware::builder(CookieSessionStore::default(), Key::from(env::var("SESSION_KEY").expect("SESSION_KEY must be set").as_bytes()))
                .cookie_secure(is_https)
                .build();

        App::new()
            .app_data(web::Data::new(pool.clone()))
            .app_data(web::Data::new(server.clone()))
            .app_data(server_state.clone())
            .wrap(IdentityMiddleware::default())
            .wrap(session_mw)
            .wrap(middleware::Logger::default())
            .service(Files::new("/static/", "static/"))
            .service(web::resource("/ws/").to(chat::server::chat_route))
            .service(routes::auth::activate)
            .service(routes::auth::login)
            .service(routes::auth::logout)
            .service(routes::auth::signup)
            .service(routes::ingest_hooks::on_connect)
            .service(routes::profile::poster::upload_poster)
            .service(routes::profile::profile::profile)
            .service(routes::profile::server_settings::update_server_info)
            .service(routes::profile::stickers::add_sticker)
            .service(routes::profile::stickers::delete_sticker)
            .service(routes::profile::stickers::rename_sticker)
            .service(routes::profile::user_settings::change_email)
            .service(routes::profile::user_settings::change_mjpeg)
            .service(routes::profile::user_settings::change_stream_key)
            .service(routes::profile::user_settings::change_username)
            .service(routes::proxy::images)
            .service(routes::proxy::stickers)
            .service(routes::public::chat)
            .service(routes::public::index)
            .service(routes::public::mjpeg)
            .service(routes::public::reset)
            .service(routes::public::video)
            .service(routes::reset::new_password)
            .service(routes::reset::new_password_callback)
            .service(routes::reset::reset)
    });

    // https://actix.rs/docs/autoreload/
    let mut listenfd = ListenFd::from_env();
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind(bind_address)?
    };

    let res = server.run().await;

    res
}
