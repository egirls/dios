use crate::utils::markdown;

#[test]
fn regular_text_unaffected() {
    assert_eq!("test", markdown::parse_inline("test"));
}

#[test]
fn inline_list() {
    assert_eq!("test", markdown::parse_inline("- test"));
}

#[test]
fn inline_header() {
    assert_eq!("test", markdown::parse_inline("# test"))
}

#[test]
fn inline_header_with_bold() {
    assert_eq!("<strong>test</strong>", markdown::parse_inline("# **test**"));
}

#[test]
fn escape_html() {
    assert_eq!("&lt;h1&gt;test&lt;/h1&gt;\n", markdown::parse_inline("<h1>test</h1>"));
}

#[test]
fn escape_nested_html() {
    assert_eq!(
        "&lt;h1&gt;&lt;strong&gt;test&lt;/strong&gt;&lt;/h1&gt;\n",
        markdown::parse_inline("<h1><strong>test</strong></h1>")
    );
}
