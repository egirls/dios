CREATE TABLE stickers (
  id VARCHAR PRIMARY KEY,
  pack VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  UNIQUE (pack, name)
)
