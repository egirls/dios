#!/bin/sh

export PORT="${MJPEG_PORT:=9090}"
export HOST=0.0.0.0
export MJPEG_SOURCE="${HLS_MANIFEST}"
echo $PORT 1>&2
echo $HOST 1>&2
echo $MJPEG_SOURCE 1>&2
PORT="${PORT}" HOST="0.0.0.0" exec "$@"
