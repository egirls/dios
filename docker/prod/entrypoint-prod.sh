#!/bin/sh

mkdir -p "${DATA_ROOT}"/stickers
mkdir -p "${DATA_ROOT}"/segments

if [ ! -f "${DATA_ROOT}"/stickers/stickers.json ]; then
  echo '{}' > "${DATA_ROOT}"/stickers/stickers.json
fi

# we need nscd so that static ffmpeg can do DNS resolution, but the package is
# broken so we have to add the missing directory and start it manually
mkdir -p /var/run/nscd
nscd

exec "$@"
