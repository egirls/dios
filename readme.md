# DIOS

The purpose of this project is to demonstrate an alternative to centralized
streaming platforms like Twitch.  Instead of a very large, self-contained
ecosystem, DIOS is aimed at **small, self-governed streaming communities.**  By
design, only one person can stream on a DIOS server at any given time – the
**inability to scale** is a feature that is necessary for any [sustainable
online community](https://runyourown.social/#keep-it-small).

While DIOS is similar in spirit to federated platforms like
[Mastodon](https://joinmastodon.org/) and [PeerTube](https://joinpeertube.org/),
federation is not planned for this project.

DIOS is currently in a very early phase of development, but an example of it in
action can be seen at <https://trash.cloud>.  Contributions are very welcome,
whether code, documentation, or simply feedback!

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [DIOS](#dios)
    - [what dios is not](#what-dios-is-not)
    - [streaming](#streaming)
        - [OBS settings](#obs-settings)
            - [NVIDIA](#nvidia)
            - [integrated](#integrated)
    - [features](#features)
        - [clips](#clips)
        - [stickers](#stickers)
        - [markdown](#markdown)
        - [mjpeg](#mjpeg)
    - [deploying DIOS](#deploying-dios)
    - [contributing](#contributing)

<!-- markdown-toc end -->


## what dios is not ##

* **a twitch clone**.  We're not trying to replicate commercial platforms like
    Twitch or Mixer, particularly those features intended to increase revenue at
    the expense of community or individual privacy. We also don't have the
    resources or interest in ticking every feature box.
* **federated**.  We don't have anything against a federated streaming service.
    Our focus is simply on supporting small communities, rather than connecting
    them.
* **licensed for capitalist use**.  Not only are our goals opposed to
    for-profit streaming platforms, capitalist use is actively forbidden by our
    license, the [Anti-Capitalist Software
    License](https://anticapitalist.software/).  While we definitely don't want
    exploitative for-profits using DIOS, if your worked-owned and democratically
    business is concerned you technically count as "for profit," drop us a line.

## streaming

To designate accounts as streamers with superadmin privileges, use
`scripts/admin.sh` and `scripts/unadmin.sh`, e.g.:
```
docker-compose exec app /dios/scripts/admin.sh 'admin@localhost'
```

Designating a user as a streamer with `scripts/admin.sh` also sets an initial
stream key; this can be changed from their profile page.

Authorized streamers can then stream from OBS using `rtmp://host.name/live` and
their stream key.

### OBS settings

Configure your streaming settings with an eye to low latency.

#### NVIDIA

We suggest something like the below for [NVIDIA
GPUs](https://www.nvidia.com/en-us/geforce/guides/broadcasting-guide/):

![OBS recommended video settings for NVIDIA](./docs/obs-nvidia.png)

#### integrated

For an integrated (Intel) GPU, we’ve seen good results with the following settings:

![OBS recommended video settings for integrated Intel](./docs/obs-integrated.png)

## features

### clips

If deployed with `CLIPS_ENABLED`, users with mod permissions can create clips
using `/clip [seconds]` in chat.  A clip of the requested length will be posted
in chat for users to download.  By default, the maximum length is 180 seconds,
and when no length is specified, the length is 30 seconds; these defaults can be
configured with environment variables.

### stickers

DIOS allows users with streamer permissions to upload custom stickers that can
be used in chat.  Stickers are tracked in the database using the `Sticker`
model, and files are written to `$DATA_ROOT/stickers`.  Supported file types
are:

- APNG
- GIF
- JPEG
- PNG
- SVG
- WEBP

### markdown

DIOS uses [comrak](https://github.com/kivikakk/comrak) to parse Markdown in chat
messages. It supports the usual suspects (**bold** using `**`, *italics* using
`*`, code using the backtick, and links using `[link text](url)`), plus
strikethrough.

It doesn't support headers, lists, or block quotes (although it would be funny
if it did).

Images (`![alt text](url)`) are supported, but they're converted to a link.

(On a technical note, comrak also takes care of escaping HTML.)

### mjpeg

This started as a [joke on
cohost](https://cohost.org/catalina/post/22802-trying-to-replicate) but now it’s
an actual feature.  If DIOS is deployed with `mjpeg.enabled=true`, each streamer
can individually turn on MJPEG streaming from their profile, and a [Motion
JPEG](https://en.wikipedia.org/wiki/Motion_JPEG) version of the stream will be
served at `/mjpeg` when they go live.

## deploying DIOS

For CI/CD and for our own instance, we deploy DIOS into
[Kubernetes](https://kubernetes.io/) using the [Helm](https://helm.sh) chart
located in [`/chart`](../chart).  Any number of other deployment strategies are
possible; see the [architecture documentation](./docs/architecture.md) for an
overview of how the components interact and the general structural requirements
of the system.

For a detailed walkthrough of our deployment configuration, see the [deployment
reference](./docs/deploying.md).

**The short version:** When deploying with Helm, default configuration variables
are listed in [`values.yaml`](./chart/values.yaml).  Override them as necessary
in your local values files:

```yaml
dios:
  app_domain: your.cool.tld
  app_title: 'not twitch'
  s3:
    use_https: true
    assets:
      access_key: cool-key
      access_secret: cool-lock
      bucket: assets
      endpoint: https://not.aws
      region: cool-zone
    clips:
      enabled: false

smtp:
  enabled: true
  from_address: hello.from@your.cool.tld
  hostname: smtp.nice
  password: dont-tell-me
  username: big-mailer

# this may be different, depending on how your ingress is configured
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  hosts:
    - host: your.cool.tld
      paths: ['/']
  tls:
    - secretName: cool-secret
      hosts:
        - your.cool.tld
```

You can then deploy with Helm:
```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
helm dep update ./chart
helm install dios ./chart -n dios --create-namespace -f your/values/file.yaml
```

## contributing

All contributions are welcome, whether documentation, code, or simply bug
reports.  See the [contributor’s reference](./docs/contributor-reference.md) for
more detailed explanations of the code, and the [architecture
documentation](./docs/architecture.md) for an overview of how the different
components interact.
