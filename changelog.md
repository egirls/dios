# 4.0.0

- S3 is now used for image assets; the main webapp no longer needs any
  persistent volumes

# 3.0.0

- complete re-write of custom emoji/stickers
  - consolidated into `Sticker` which is modelled in the database
  - GIF, JPEG, WEBP, APNG, and SVG now supported alongside PNG
  - to migrate existing stickers, use `scripts/migrate-stickers.sh` after
    running the new DB migrations

# 2.0.0

in the beginning there was darkness
